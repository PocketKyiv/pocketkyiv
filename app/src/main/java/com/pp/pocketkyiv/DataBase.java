package com.pp.pocketkyiv;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.pp.pocketkyiv.models.AddressModel;
import com.pp.pocketkyiv.models.CategoryModel;
import com.pp.pocketkyiv.models.OrganizationModel;

import java.util.ArrayList;

public class DataBase {

    private static DatabaseHelper openHelper = new DatabaseHelper();

    public static final String CATEGORIES = "categories";
    public static final String ORGANIZATIONS = "organizations";
    public static final String ADDRESSES = "addresses";

    public static final String CATEGORY_ID = "category_id";
    public static final String PARENT_ID = "parent_id";
    public static final String TITLE = "title";
    public static final String TITLE_LOWER = "title_lower";
    public static final String HREF = "href";

    public static final String ADDRESS_ID = "address_id";
    public static final String ADDRESS = "address";
    public static final String FORMATTED_ADDRESS = "formatted_address";
    public static final String LAST_UPDATE = "last_update";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public static final String ORGANIZATION_ID = "organization_id";
    public static final String LOGO = "logo";
    public static final String EMAIL = "email";
    public static final String SITE = "site";
    public static final String WORK_TIME = "work_time";
    public static final String CONTACT = "contact";
    public static final String PHONE_NUMBERS = "phone_numbers";
    public static final String NUMBER = "number";
    public static final String FAVORITE = "favorite";
    public static final String NEXT_START = "next_start";

    public static SQLiteDatabase getDatabase() {
        return openHelper.getWritableDatabase();
    }

    public static void writeCategories(ArrayList<CategoryModel> models) {
        SQLiteDatabase db = openHelper.getWritableDatabase();
        db.beginTransaction();

        for(int i = 0; i < models.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DataBase.CATEGORY_ID, models.get(i).category_id);
            contentValues.put(DataBase.PARENT_ID, models.get(i).parent_id);
            contentValues.put(DataBase.TITLE, models.get(i).title);
            contentValues.put(DataBase.HREF, models.get(i).href);

            Log.d("test", "category=" + i);
            db.insert(DataBase.CATEGORIES, null, contentValues);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public static void writeOrganizations(ArrayList<OrganizationModel> models) {
        SQLiteDatabase db = openHelper.getWritableDatabase();
        db.beginTransaction();

        for(int i = 0; i < models.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DataBase.CATEGORY_ID, models.get(i).category_id);
            contentValues.put(DataBase.ORGANIZATION_ID, models.get(i).organization_id);
            contentValues.put(DataBase.TITLE, models.get(i).title);
            contentValues.put(DataBase.TITLE_LOWER, models.get(i).title.toLowerCase());
            contentValues.put(DataBase.HREF, models.get(i).href);
            contentValues.put(DataBase.LAST_UPDATE, models.get(i).last_update);
            contentValues.put(DataBase.LOGO, models.get(i).logo);
            contentValues.put(DataBase.EMAIL, models.get(i).email);
            contentValues.put(DataBase.SITE, models.get(i).site);
            contentValues.put(DataBase.WORK_TIME, models.get(i).work_time);
            contentValues.put(DataBase.CONTACT, models.get(i).contact);
            contentValues.put(DataBase.FAVORITE, models.get(i).favorite);
            contentValues.put(DataBase.NEXT_START, models.get(i).next_start);
            contentValues.put(DataBase.PHONE_NUMBERS, models.get(i).phonesToJSON(models.get(i).phone_numbers).toString());

            Log.d("test", "org=" + i);
            db.insert(DataBase.ORGANIZATIONS, null, contentValues);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public static void writeAddresses(ArrayList<AddressModel> models) {
        SQLiteDatabase db = openHelper.getWritableDatabase();
        db.beginTransaction();

        for(int i = 0; i < models.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DataBase.CATEGORY_ID, models.get(i).category_id);
            contentValues.put(DataBase.ORGANIZATION_ID, models.get(i).organization_id);
            contentValues.put(DataBase.ADDRESS_ID, models.get(i).address_id);
            contentValues.put(DataBase.ADDRESS, models.get(i).address);
            contentValues.put(DataBase.LONGITUDE, models.get(i).longitude);
            contentValues.put(DataBase.LATITUDE, models.get(i).latitude);
            contentValues.put(DataBase.LAST_UPDATE, models.get(i).last_update);
            contentValues.put(DataBase.FORMATTED_ADDRESS, models.get(i).formatted_address);

            Log.d("test", "address=" + i);
            db.insert(DataBase.ADDRESSES, null, contentValues);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public static long getCurrentDateUpdate(String table) {
        long currentDate = Long.MAX_VALUE;
        SQLiteDatabase db = openHelper.getWritableDatabase();
        Cursor cursor = db.rawQuery("SELECT MAX(" + LAST_UPDATE + ") FROM " + table, null);

        int last = cursor.getColumnIndex("MAX(" + LAST_UPDATE + ")");
        if(cursor.moveToFirst())
            currentDate = cursor.getLong(last);

        cursor.close();
        return currentDate;
    }

    public static void updateOrganizations(ArrayList<OrganizationModel> models) {
        SQLiteDatabase db = openHelper.getWritableDatabase();

        int id = 0;
        Cursor max = db.rawQuery("SELECT MAX(" + ORGANIZATION_ID + ") FROM " + ORGANIZATIONS, null);
        int last = max.getColumnIndex("MAX(" + ORGANIZATION_ID + ")");

        if(max.moveToFirst())
            id = max.getInt(last);

        max.close();
        db.beginTransaction();

        for(int i = 0; i < models.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DataBase.CATEGORY_ID, models.get(i).category_id);
            contentValues.put(DataBase.ORGANIZATION_ID, models.get(i).organization_id);
            contentValues.put(DataBase.TITLE, models.get(i).title);
            contentValues.put(DataBase.TITLE_LOWER, models.get(i).title.toLowerCase());
            contentValues.put(DataBase.HREF, models.get(i).href);
            contentValues.put(DataBase.LAST_UPDATE, models.get(i).last_update);
            contentValues.put(DataBase.LOGO, models.get(i).logo);
            contentValues.put(DataBase.EMAIL, models.get(i).email);
            contentValues.put(DataBase.SITE, models.get(i).site);
            contentValues.put(DataBase.WORK_TIME, models.get(i).work_time);
            contentValues.put(DataBase.CONTACT, models.get(i).contact);
            contentValues.put(DataBase.FAVORITE, models.get(i).favorite);
            contentValues.put(DataBase.NEXT_START, models.get(i).next_start);
            contentValues.put(DataBase.PHONE_NUMBERS, models.get(i).phonesToJSON(models.get(i).phone_numbers).toString());

            Log.d("test", "org=" + i);
            if(models.get(i).organization_id > id)
                db.insert(DataBase.ORGANIZATIONS, null, contentValues);
            else
                db.update(DataBase.ORGANIZATIONS, contentValues, DataBase.ORGANIZATION_ID + " = " +
                        models.get(i).organization_id, null);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    public static void updateAddresses(ArrayList<AddressModel> models) {
        SQLiteDatabase db = openHelper.getWritableDatabase();

        int id = 0;
        Cursor max = db.rawQuery("SELECT MAX(" + ADDRESS_ID + ") FROM " + ADDRESSES, null);
        int last = max.getColumnIndex("MAX(" + ADDRESS_ID + ")");

        if(max.moveToFirst())
            id = max.getInt(last);

        max.close();
        db.beginTransaction();

        for(int i = 0; i < models.size(); i++) {
            ContentValues contentValues = new ContentValues();
            contentValues.put(DataBase.CATEGORY_ID, models.get(i).category_id);
            contentValues.put(DataBase.ORGANIZATION_ID, models.get(i).organization_id);
            contentValues.put(DataBase.ADDRESS_ID, models.get(i).address_id);
            contentValues.put(DataBase.ADDRESS, models.get(i).address);
            contentValues.put(DataBase.LONGITUDE, models.get(i).longitude);
            contentValues.put(DataBase.LATITUDE, models.get(i).latitude);
            contentValues.put(DataBase.LAST_UPDATE, models.get(i).last_update);
            contentValues.put(DataBase.FORMATTED_ADDRESS, models.get(i).formatted_address);

            Log.d("test", "address=" + i);
            if(models.get(i).address_id > id)
                db.insert(DataBase.ADDRESSES, null, contentValues);
            else
                db.update(DataBase.ADDRESSES, contentValues, DataBase.ADDRESS_ID + " = " +
                        models.get(i).address_id, null);
        }

        db.setTransactionSuccessful();
        db.endTransaction();
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        private DatabaseHelper() {
            super(App.getAppContext(), "dataBase", null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL("create table " + CATEGORIES + " ("
                    + CATEGORY_ID + " integer,"
                    + PARENT_ID + " integer,"
                    + TITLE + " text,"
                    + HREF + " text);");

            db.execSQL("create table " + ADDRESSES + " ("
                    + ADDRESS_ID + " integer,"
                    + CATEGORY_ID + " integer,"
                    + ORGANIZATION_ID + " integer,"
                    + ADDRESS + " text,"
                    + FORMATTED_ADDRESS + " text,"
                    + LATITUDE + " text,"
                    + LONGITUDE + " text,"
                    + LAST_UPDATE + " text);");

            db.execSQL("create table " + ORGANIZATIONS + " ("
                    + ORGANIZATION_ID + " integer,"
                    + CATEGORY_ID + " integer,"
                    + TITLE + " text,"
                    + TITLE_LOWER + " text,"
                    + HREF + " text,"
                    + LAST_UPDATE + " text,"
                    + LOGO + " text,"
                    + EMAIL + " text,"
                    + SITE + " text,"
                    + WORK_TIME + " text,"
                    + CONTACT + " text,"
                    + FAVORITE + " integer,"
                    + NEXT_START + " integer,"
                    + PHONE_NUMBERS + " text);");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}
