package com.pp.pocketkyiv;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.pp.pocketkyiv.blur.BlurBehind;
import com.pp.pocketkyiv.models.OrganizationModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class OrganizationActivity extends Activity {

    private ArrayList<String> phone_names;
    private ArrayList<String> phones;

    private LinearLayout parentLayout;

    private RelativeLayout org_layout;
    private ImageButton org_back;

    private String email = null;
    private String site = null;

    private int item_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_organization);

        BlurBehind.getInstance().withAlpha(100)
                .withFilterColor(Color.parseColor("#5e5e5e"))
                .setBackground(this);

        phones = new ArrayList<>();
        phone_names = new ArrayList<>();
        parentLayout = (LinearLayout) findViewById(R.id.numbersFill);

        Intent intent = getIntent();
        item_id = intent.getIntExtra("id", 0);

        org_layout = (RelativeLayout) findViewById(R.id.org_layout);
        org_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        org_back = (ImageButton) findViewById(R.id.org_back);
        org_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextView tittle = (TextView) findViewById(R.id.activity_organization_name);
        tittle.setText(intent.getStringExtra("subcategory_item_name"));
        readDataBase(item_id);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        for (int i = 0; i < phones.size(); i++) {

            if (i < 2) {
                View view_phone = inflater.inflate(R.layout.phone_item, null, false);

                final TextView phone = (TextView) view_phone.findViewById(R.id.phone);
                phone.setText(phones.get(i));

                TextView phone_name = (TextView) view_phone.findViewById(R.id.phone_name);
                phone_name.setText(phone_names.get(i));

                view_phone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        String telefon = "tel:" + phone.getText().toString();
                        callIntent.setData(Uri.parse(telefon));

                        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }

                        startActivity(callIntent);
                    }
                });

                parentLayout.addView(view_phone);
            }
        }

        if(email != null && !email.equals("")) {
            View view_email = inflater.inflate(R.layout.mail_item, null, false);
            ((TextView) view_email.findViewById(R.id.mail)).setText(email);
            view_email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_SEND);
                    intent.setType("message/rfc822");
                    intent.putExtra(Intent.EXTRA_EMAIL, email);
                    startActivity(Intent.createChooser(intent, "Send Email"));
                }
            });

            parentLayout.addView(view_email);
        }

        if(site != null && !site.equals("")) {
            View view_email = inflater.inflate(R.layout.link_item, null, false);
            try {
                ((TextView) view_email.findViewById(R.id.web)).setText(new URL(site).getHost());
            } catch (MalformedURLException e) {
                e.printStackTrace();
                ((TextView) view_email.findViewById(R.id.web)).setText(site);
            }

            view_email.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setData(Uri.parse(site));
                    startActivity(i);
                }
            });

            parentLayout.addView(view_email);
        }

        View view_description = inflater.inflate(R.layout.description_item, null, false);
        view_description.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentInf = new Intent(OrganizationActivity.this, InformationActivity.class);
                intentInf.putExtra("id", item_id);
                startActivity(intentInf);
            }
        });

        parentLayout.addView(view_description);

        final double my_lat = intent.getDoubleExtra("my_lat", 0);
        final double my_lon = intent.getDoubleExtra("my_lon", 0);
        final double lat = intent.getDoubleExtra("lat", 0);
        final double lon = intent.getDoubleExtra("lon", 0);

        findViewById(R.id.address).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + my_lat + "," + my_lon +
                                "&daddr=" + lat + "," + lon));
                startActivity(intent);
            }
        });
    }

    private void readDataBase(int subcategory_item_id) {
        SQLiteDatabase database = DataBase.getDatabase();

        Cursor cursor = database.query(DataBase.ORGANIZATIONS, null,
                DataBase.ORGANIZATION_ID + " = " + Integer.toString(subcategory_item_id), null, null, null, null);

        if(cursor.moveToFirst()) {
            int phone = cursor.getColumnIndex(DataBase.PHONE_NUMBERS);
            int em = cursor.getColumnIndex(DataBase.EMAIL);
            int si = cursor.getColumnIndex(DataBase.SITE);

            email = cursor.getString(em);
            site = cursor.getString(si);

            try {
                ArrayList<OrganizationModel.Phone> phonesModel  = OrganizationModel
                        .parsePhones(new JSONArray(cursor.getString(phone)));
                for (OrganizationModel.Phone ph : phonesModel) {
                    phones.add(ph.number);
                    phone_names.add(ph.title);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Cursor cursor_address = database.query(DataBase.ADDRESSES, null,
                DataBase.ORGANIZATION_ID + " = " + Integer.toString(subcategory_item_id), null, null, null, null);

        if(cursor_address.moveToFirst()) {
            int addr = cursor_address.getColumnIndex(DataBase.FORMATTED_ADDRESS);
            ((TextView) findViewById(R.id.address)).setText(cursor_address.getString(addr));
        }

        cursor.close();
        cursor_address.close();
        database.close();
    }
}
