package com.pp.pocketkyiv;

import android.app.Application;
import android.content.Context;

public class App extends Application {

    private static Context appContext;
    private static boolean permissionRequested = false;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = this;
    }

    public static Context getAppContext() {
        return appContext;
    }

    public static void setPermissionRequested(boolean state) {
        permissionRequested = state;
    }

    public static boolean getPermissionRequested() {
        return permissionRequested;
    }
}
