package com.pp.pocketkyiv;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.pp.pocketkyiv.adapters.PagerAdapter;
import com.pp.pocketkyiv.fragments.ContentPage;
import com.pp.pocketkyiv.fragments.FavoritesPage;
import com.pp.pocketkyiv.fragments.SubcategoriesPage;
import com.pp.pocketkyiv.interfaces.IOnLastUpdateChange;
import com.pp.pocketkyiv.managers.ApiManager;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends AppCompatActivity implements ContentPage.OnChildClickListener {

    public static final String DATE = "date";

    private int tasks = 0;
    private boolean find_state;
    private boolean input_state;

    private PagerAdapter mSectionsPagerAdapter;
    private ViewPager mViewPager;

    private ImageButton menuButton;
    private ImageButton backButton;
    private Toolbar toolbar;
    private EditText findField;

    private AlertDialog first_dialog;
    private ProgressDialog progressDialog;
    private AlertDialog.Builder builder;
    private AlertDialog.Builder builder_first;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initView();
        checkFirstRun();
    }

    private void initView() {
        setContentView(R.layout.activity_main);

        find_state = false;
        input_state = false;

        toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        progressDialog = new ProgressDialog(this);
        builder = new AlertDialog.Builder(this);
        builder_first = new AlertDialog.Builder(this);

        progressDialog.setTitle("Обновление справочника");
        progressDialog.setMessage("Подождите...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMax(100);

        menuButton = (ImageButton)findViewById(R.id.menuButton);
        backButton = (ImageButton)findViewById(R.id.backButton);
        findField = (EditText)findViewById(R.id.editText);

        mSectionsPagerAdapter = new PagerAdapter(getSupportFragmentManager(), getApplicationContext());
        mViewPager = (ViewPager) findViewById(R.id.container);

        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setOffscreenPageLimit(3);
        mViewPager.setCurrentItem(1);
    }

    public void checkFirstRun() {
        boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (isFirstRun){

            builder_first.setTitle("Обновление");
            builder_first.setMessage("Обновить номера справочника?");
            builder_first.setPositiveButton("Да", myClickListener);
            builder_first.setNegativeButton("Нет", myClickListener);

            first_dialog = builder_first.create();
            first_dialog.show();

            getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                    .edit()
                    .putBoolean("isFirstRun", false)
                    .apply();
        }
    }

    DialogInterface.OnClickListener myClickListener = new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
            switch (which) {
                case Dialog.BUTTON_POSITIVE:
                    first_dialog.dismiss();
                    tasks = 0;

                    ApiManager.getInstance().getCategories(MainActivity.this, new Runnable() {
                        @Override
                        public void run() {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    tasks++;
                                    progressDialog.incrementProgressBy(33);
                                    checkDownloadEnd();
                                }
                            });
                        }
                    });

                    ApiManager.getInstance().getOrganizations(MainActivity.this, new Runnable() {
                        @Override
                        public void run() {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    tasks++;
                                    progressDialog.incrementProgressBy(33);
                                    checkDownloadEnd();
                                }
                            });
                        }
                    });

                    ApiManager.getInstance().getAddresses(MainActivity.this, new Runnable() {
                        @Override
                        public void run() {
                            new Handler(Looper.getMainLooper()).post(new Runnable() {
                                @Override
                                public void run() {
                                    tasks++;
                                    progressDialog.incrementProgressBy(33);
                                    checkDownloadEnd();
                                }
                            });
                        }
                    });

                    progressDialog.show();
                    break;

                case Dialog.BUTTON_NEGATIVE:
                    first_dialog.dismiss();
                    break;

                case Dialog.BUTTON_NEUTRAL:
                    break;
            }
        }
    };

    private void checkDownloadEnd() {
        if(tasks >= 3) {
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
            writeDataBaseUpdate(DateFormat
                    .getDateInstance(SimpleDateFormat.LONG, new Locale("ru")).format(new Date()) + " в " +
                    formatter.format(new Date()));
            progressDialog.dismiss();
            finish();
            startActivity(getIntent());
        }
    }

    private void checkUpdateEnd() {
        if(tasks >= 2) {
            SimpleDateFormat formatter = new SimpleDateFormat("HH:mm");
            writeDataBaseUpdate(DateFormat
                    .getDateInstance(SimpleDateFormat.LONG, new Locale("ru")).format(new Date()) + " в " +
                    formatter.format(new Date()));
            progressDialog.dismiss();
            finish();
            startActivity(getIntent());
        }
    }

    @Override
    public void childClicked(String name, int id) {
        mViewPager.setCurrentItem(2);
        mSectionsPagerAdapter.setNames(name);
        SubcategoriesPage page = (SubcategoriesPage)mSectionsPagerAdapter.getItem(2);
        page.updateFragment(id);
    }

    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.menu_main, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener(){
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                int id = item.getItemId();

                switch (id) {
                    case R.id.action_refresh:

                        ApiManager.getInstance().getLastUpdate(MainActivity.this, new IOnLastUpdateChange() {
                            @Override
                            public void lastUpdateDate(final long organization, final long address) {
                                long currentOrg = DataBase.getCurrentDateUpdate(DataBase.ORGANIZATIONS);
                                long currentAddress = DataBase.getCurrentDateUpdate(DataBase.ADDRESSES);

                                if(currentOrg == 0 || currentAddress == 0) {
                                    tasks = 0;

                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            ApiManager.getInstance().getCategories(MainActivity.this, new Runnable() {
                                                @Override
                                                public void run() {
                                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            tasks++;
                                                            progressDialog.incrementProgressBy(33);
                                                            checkDownloadEnd();
                                                        }
                                                    });
                                                }
                                            });

                                            ApiManager.getInstance().getOrganizations(MainActivity.this, new Runnable() {
                                                @Override
                                                public void run() {
                                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            tasks++;
                                                            progressDialog.incrementProgressBy(33);
                                                            checkDownloadEnd();
                                                        }
                                                    });
                                                }
                                            });

                                            ApiManager.getInstance().getAddresses(MainActivity.this, new Runnable() {
                                                @Override
                                                public void run() {
                                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            tasks++;
                                                            progressDialog.incrementProgressBy(33);
                                                            checkDownloadEnd();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });

                                } else if((organization - currentOrg) >= 10800 ||
                                            (address - currentAddress) >= 10800) {
                                    tasks = 0;

                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {

                                            ApiManager.getInstance().updateOrganizations(
                                                    MainActivity.this, organization, new Runnable() {
                                                @Override
                                                public void run() {
                                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            tasks++;
                                                            progressDialog.incrementProgressBy(50);
                                                            checkUpdateEnd();
                                                        }
                                                    });
                                                }
                                            });

                                            ApiManager.getInstance().updateAddresses(
                                                    MainActivity.this, address, new Runnable() {
                                                @Override
                                                public void run() {
                                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            tasks++;
                                                            progressDialog.incrementProgressBy(50);
                                                            checkUpdateEnd();
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });

                                } else {
                                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                                        @Override
                                        public void run() {
                                            progressDialog.dismiss();

                                            builder.setTitle("Информация");
                                            builder.setMessage("Ваша база находится в удовлетворительном состоянии.");
                                            builder.setNeutralButton("Oк", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    dialog.dismiss();
                                                }
                                            });

                                            builder.create().show();
                                        }
                                    });
                                }
                            }
                        });

                        progressDialog.show();

                        break;
                    case R.id.action_refresh_count:

                        builder.setTitle("Дата последнего обновления");
                        builder.setMessage(readDataBaseUpdate());
                        builder.setNeutralButton("Oк", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });

                        builder.create().show();
                        break;

                    default:
                        break;
                }

                return false;
            }
        });

        popup.show();
    }

    public String readDataBaseUpdate() {
        return getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                .getString(DATE, "1 января 2017 в 12:12");
    }

    public void writeDataBaseUpdate(String date) {
        Log.d("preference", "date = " + date);
        getSharedPreferences("PREFERENCE", MODE_PRIVATE).edit().putString(DATE, date).apply();
    }

    public void showFind(View v) {
        if (!find_state && !input_state) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            menuButton.setVisibility(View.GONE);
            backButton.setVisibility(View.VISIBLE);
            findField.setVisibility(View.VISIBLE);
            find_state = true;

            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

            if (findField.requestFocus()) {
                getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }

        }  else {
            find_state = false;
            input_state = true;
            mViewPager.setCurrentItem(2);
            mSectionsPagerAdapter.setNames("Поиск");
            SubcategoriesPage page = (SubcategoriesPage)mSectionsPagerAdapter.getItem(2);
            page.updateFragment(findField.getText().toString());
//            page.updateBanner(findField.getText().toString());

            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);

//            findField.setText("");
        }
    }

    public void showBack(View v) {
        find_state = false;
        input_state = false;
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        menuButton.setVisibility(View.VISIBLE);
        backButton.setVisibility(View.GONE);
        findField.setVisibility(View.GONE);
        findField.setText("");

        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public void updateFavorites() {
        FavoritesPage page = (FavoritesPage) mSectionsPagerAdapter.getItem(0);
        page.updatePage();
    }

    public void updateSubcategories(int id) {
        SubcategoriesPage page = (SubcategoriesPage) mSectionsPagerAdapter.getItem(2);
        page.updateFragment(id);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        mSectionsPagerAdapter.getItem(0).onRequestPermissionsResult(requestCode, permissions, grantResults);
        mSectionsPagerAdapter.getItem(2).onRequestPermissionsResult(requestCode, permissions, grantResults);
    }
}
