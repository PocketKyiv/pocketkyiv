package com.pp.pocketkyiv;

public class Const {

    public static int PERMISSION_LOCATION = 1;

    public static class OrganizationState {
        public static int FAVORITE = 1;
        public static int NORMAL = 0;
    }

    public static class NextStart {
        public static int DELETE_FROM_FAVORITE = 1;
        public static int NORMAL = 0;
    }
}
