package com.pp.pocketkyiv.models;

import com.pp.pocketkyiv.DataBase;

import org.json.JSONException;
import org.json.JSONObject;

public class CategoryModel {

    public int category_id;
    public int parent_id;
    public String title;
    public String href;

    public CategoryModel(JSONObject category) throws JSONException {
        if(category.has(DataBase.CATEGORY_ID) && category.has(DataBase.PARENT_ID)) {
            category_id = category.getInt(DataBase.CATEGORY_ID);
            parent_id = category.getInt(DataBase.PARENT_ID);

            if (category.has(DataBase.TITLE))
                title = category.getString(DataBase.TITLE);

            if (category.has(DataBase.HREF))
                href = category.getString(DataBase.HREF);
        }
    }
}
