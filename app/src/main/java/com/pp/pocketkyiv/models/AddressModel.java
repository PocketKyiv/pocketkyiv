package com.pp.pocketkyiv.models;

import com.pp.pocketkyiv.DataBase;

import org.json.JSONException;
import org.json.JSONObject;

public class AddressModel {

    public int address_id;
    public int category_id;
    public int organization_id;
    public String address;
    public String formatted_address;
    public String latitude;
    public String longitude;
    public String last_update;

    public AddressModel(JSONObject address) throws JSONException {
        if(address.has(DataBase.CATEGORY_ID) &&
                address.has(DataBase.ORGANIZATION_ID) && address.has(DataBase.ADDRESS_ID)) {
            category_id = address.getInt(DataBase.CATEGORY_ID);
            organization_id = address.getInt(DataBase.ORGANIZATION_ID);
            address_id = address.getInt(DataBase.ADDRESS_ID);

            if (address.has(DataBase.ADDRESS))
                this.address = address.getString(DataBase.ADDRESS);

            if (address.has(DataBase.FORMATTED_ADDRESS))
                formatted_address = address.getString(DataBase.FORMATTED_ADDRESS);

            if(address.has(DataBase.LAST_UPDATE))
                last_update = address.getString(DataBase.LAST_UPDATE);

            if(address.has(DataBase.LATITUDE))
                latitude = address.getString(DataBase.LATITUDE);

            if(address.has(DataBase.LONGITUDE))
                longitude = address.getString(DataBase.LONGITUDE);
        }
    }
}
