package com.pp.pocketkyiv.models;

import com.pp.pocketkyiv.Const;
import com.pp.pocketkyiv.DataBase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class OrganizationModel {

    public int organization_id;
    public int category_id;
    public int favorite;
    public int next_start;
    public String title;
    public String href;
    public String last_update;
    public String logo;
    public String email;
    public String site;
    public String work_time;
    public String contact;
    public ArrayList<Phone> phone_numbers;

    public OrganizationModel(JSONObject organization) throws JSONException {
        if(organization.has(DataBase.CATEGORY_ID) && organization.has(DataBase.ORGANIZATION_ID)) {
            category_id = organization.getInt(DataBase.CATEGORY_ID);
            organization_id = organization.getInt(DataBase.ORGANIZATION_ID);
            favorite = Const.OrganizationState.NORMAL;
            next_start = Const.NextStart.NORMAL;

            if (organization.has(DataBase.TITLE))
                title = organization.getString(DataBase.TITLE);

            if (organization.has(DataBase.HREF))
                href = organization.getString(DataBase.HREF);

            if(organization.has(DataBase.LAST_UPDATE))
                last_update = organization.getString(DataBase.LAST_UPDATE);

            if(organization.has(DataBase.LOGO))
                logo = organization.getString(DataBase.LOGO);

            if(organization.has(DataBase.EMAIL))
                email = organization.getString(DataBase.EMAIL);

            if(organization.has(DataBase.SITE))
                site = organization.getString(DataBase.SITE);

            if(organization.has(DataBase.WORK_TIME))
                work_time = organization.getString(DataBase.WORK_TIME);

            if (organization.has(DataBase.CONTACT))
                contact = organization.getString(DataBase.CONTACT);

            if(organization.has(DataBase.PHONE_NUMBERS))
                phone_numbers = parsePhones(organization.getJSONArray(DataBase.PHONE_NUMBERS));
        }
    }

    public static ArrayList<Phone> parsePhones(JSONArray phones) throws JSONException {
        ArrayList<Phone> phone_numbers = new ArrayList<>();

        for(int i = 0; i < phones.length(); i++) {
            JSONObject phone = phones.getJSONObject(i);
            phone_numbers.add(new Phone(phone));
        }

        return phone_numbers;
    }

    public static JSONArray phonesToJSON(ArrayList<Phone> phone_numbers) {
        JSONArray jsonArray = new JSONArray();

        try{
            for(int i = 0; i < phone_numbers.size(); i++) {
                jsonArray.put(phone_numbers.get(i).toJSON());
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonArray;
    }

    public static class Phone {
        public String number;
        public String title;

        public Phone(JSONObject phone) throws JSONException {
            if(phone.has(DataBase.TITLE))
                title = phone.getString(DataBase.TITLE);

            if(phone.has(DataBase.NUMBER))
                number = phone.getString(DataBase.NUMBER);
        }

        public JSONObject toJSON() throws JSONException {
            JSONObject phone = new JSONObject();
            phone.put(DataBase.TITLE, title);
            phone.put(DataBase.NUMBER, number);

            return phone;
        }
    }
}
