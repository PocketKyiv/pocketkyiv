package com.pp.pocketkyiv.fragments;

import android.app.Activity;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.pp.pocketkyiv.DataBase;
import com.pp.pocketkyiv.R;
import com.pp.pocketkyiv.adapters.ExpListViewContentAdapter;

import java.util.ArrayList;

public class ContentPage extends Fragment {

    private ArrayList<String> categories;
    private ArrayList<ArrayList<String>> subCategories;
    private ArrayList<ArrayList<Integer>> subCategories_id;
    private OnChildClickListener listener;

    public ContentPage() {

    }

    public interface OnChildClickListener {
        void childClicked(String name, int id);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            listener = (OnChildClickListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_content, container, false);
        ExpandableListView listView = (ExpandableListView) rootView.findViewById(R.id.expandableListView);

        categories = new ArrayList<>();
        subCategories = new ArrayList<>();
        subCategories_id = new ArrayList<>();

        readDataBase();

        final ExpListViewContentAdapter adapter = new ExpListViewContentAdapter(getActivity(), subCategories, categories);
        listView.setAdapter(adapter);

        listView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition,
                                        int childPosition, long id) {

                listener.childClicked(adapter.getChild(groupPosition, childPosition).toString(),
                        subCategories_id.get(groupPosition).get(childPosition));
                return false;
            }
        });

        return rootView;
    }

    private void readDataBase() {
        SQLiteDatabase database = DataBase.getDatabase();
        Cursor cursor = database.query(DataBase.CATEGORIES, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            int parent = cursor.getColumnIndex(DataBase.PARENT_ID);
            int category = cursor.getColumnIndex(DataBase.TITLE);

            do {
                if(cursor.getInt(parent) == 0)
                    categories.add(cursor.getString(category));

            } while (cursor.moveToNext());
        }

        for (int i = 0; i < categories.size(); i++) {
            subCategories.add(new ArrayList<String>());
            subCategories_id.add(new ArrayList<Integer>());
        }

        if (cursor.moveToFirst()) {
            int category_id = cursor.getColumnIndex(DataBase.CATEGORY_ID);
            int parent = cursor.getColumnIndex(DataBase.PARENT_ID);
            int category = cursor.getColumnIndex(DataBase.TITLE);

            do {
                int index = cursor.getInt(parent);

                if(index != 0) {
                    subCategories.get(index - 1).add(cursor.getString(category));
                    subCategories_id.get(index - 1).add(cursor.getInt(category_id));
                }

            } while (cursor.moveToNext());
        }

        cursor.close();
        database.close();
    }
}