package com.pp.pocketkyiv.fragments;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.pp.pocketkyiv.App;
import com.pp.pocketkyiv.Const;
import com.pp.pocketkyiv.DataBase;
import com.pp.pocketkyiv.OrganizationActivity;
import com.pp.pocketkyiv.R;
import com.pp.pocketkyiv.adapters.ListViewSubcategoryAdapter;
import com.pp.pocketkyiv.blur.BlurBehind;
import com.pp.pocketkyiv.blur.OnBlurCompleteListener;

import java.util.ArrayList;
import java.util.Collections;

public class SubcategoriesPage extends Fragment {

    private ListView listView;
    private ArrayList<Boolean> button_state;
    private ArrayList<String> organization;
    private ArrayList<Integer> organization_id;
    private ArrayList<Double> distance;
    private ArrayList<String> addresses;
    private ArrayList<Double> latitudes;
    private ArrayList<Double> longitudes;

    private ImageView logoImage;
    private int banner_id;

    double latitude = 0;
    double longitude = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_subcategories, container, false);

        listView = (ListView) rootView.findViewById(R.id.organizate);
        logoImage = (ImageView) rootView.findViewById(R.id.baner);
        button_state = new ArrayList<>();
        organization = new ArrayList<>();
        organization_id = new ArrayList<>();
        distance = new ArrayList<>();
        latitudes = new ArrayList<>();
        longitudes = new ArrayList<>();
        addresses = new ArrayList<>();

        checkLocation();

        return rootView;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == Const.PERMISSION_LOCATION) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkLocation();
            } else {
                getActivity().finish();
            }
        }
    }

    private void checkLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(App.getAppContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(App.getAppContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if(!App.getPermissionRequested()) {
                App.setPermissionRequested(true);
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        Const.PERMISSION_LOCATION);
            }

        } else {

            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }
    }

    public void updateFragment(int id) {
        button_state.clear();
        organization.clear();
        organization_id.clear();
        logoImage.setImageResource(0);
        distance.clear();
        latitudes.clear();
        longitudes.clear();
        addresses.clear();

        readDateBase(id);
//        readImageFromDataBase();

        ListViewSubcategoryAdapter adapter = new ListViewSubcategoryAdapter(getActivity(), organization,
                button_state, distance, addresses);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                BlurBehind.getInstance().execute(getActivity(), new OnBlurCompleteListener() {
                    @Override
                    public void onBlurComplete() {
                        Intent intent = new Intent(getActivity(), OrganizationActivity.class);
                        intent.putExtra("subcategory_item_name", organization.get(position));
                        intent.putExtra("id", organization_id.get(position));
                        intent.putExtra("my_lat", latitude);
                        intent.putExtra("my_lon", longitude);
                        intent.putExtra("lat", latitudes.get(position));
                        intent.putExtra("lon", longitudes.get(position));

                        startActivity(intent);
                    }
                });
            }
        });
    }

    public void updateFragment(String name) {
        button_state.clear();
        organization.clear();
        organization_id.clear();
        logoImage.setImageResource(0);
        distance.clear();
        latitudes.clear();
        longitudes.clear();
        addresses.clear();

        readDateBase(name);

        ListViewSubcategoryAdapter adapter = new ListViewSubcategoryAdapter(getActivity(), organization,
                button_state, distance, addresses);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                BlurBehind.getInstance().execute(getActivity(), new OnBlurCompleteListener() {
                    @Override
                    public void onBlurComplete() {
                        Intent intent = new Intent(getActivity(), OrganizationActivity.class);
                        intent.putExtra("subcategory_item_name", organization.get(position));
                        intent.putExtra("id", organization_id.get(position));
                        intent.putExtra("my_lat", latitude);
                        intent.putExtra("my_lon", longitude);
                        intent.putExtra("lat", latitudes.get(position));
                        intent.putExtra("lon", longitudes.get(position));

                        startActivity(intent);
                    }
                });
            }
        });
    }

    private void readDateBase(int id) {
        SQLiteDatabase database = DataBase.getDatabase();

        Cursor cursor = database.query(DataBase.ORGANIZATIONS + " as " + DataBase.ORGANIZATIONS + " inner join " +
                DataBase.ADDRESSES + " as " + DataBase.ADDRESSES + " on " +
                DataBase.ORGANIZATIONS + "." + DataBase.ORGANIZATION_ID + " = " +
                DataBase.ADDRESSES + "." + DataBase.ORGANIZATION_ID, // tables end

                new String[]{DataBase.ORGANIZATIONS + "." + DataBase.TITLE + " as " + DataBase.TITLE,
                    DataBase.ORGANIZATIONS + "." + DataBase.FAVORITE + " as " + DataBase.FAVORITE,
                    DataBase.ORGANIZATIONS + "." + DataBase.ORGANIZATION_ID + " as " + DataBase.ORGANIZATION_ID,
                    DataBase.ORGANIZATIONS + "." + DataBase.CATEGORY_ID + " as " + DataBase.CATEGORY_ID,
                    DataBase.ADDRESSES + "." + DataBase.FORMATTED_ADDRESS + " as " + DataBase.ADDRESS,
                    DataBase.ADDRESSES + "." + DataBase.LATITUDE + " as " + DataBase.LATITUDE,
                    DataBase.ADDRESSES + "." + DataBase.LONGITUDE + " as " + DataBase.LONGITUDE}, // columns end

                DataBase.ORGANIZATIONS + "." + DataBase.CATEGORY_ID + " = " + Integer.toString(id), null, null, null, null);

        if(cursor.moveToFirst()) {
            int name = cursor.getColumnIndex(DataBase.TITLE);
            int type = cursor.getColumnIndex(DataBase.FAVORITE);
            int org_id = cursor.getColumnIndex(DataBase.ORGANIZATION_ID);
            int lat = cursor.getColumnIndex(DataBase.LATITUDE);
            int lon = cursor.getColumnIndex(DataBase.LONGITUDE);
            int addr = cursor.getColumnIndex(DataBase.ADDRESS);

            do {
                organization.add(cursor.getString(name));
                organization_id.add(cursor.getInt(org_id));
                addresses.add(cursor.getString(addr));
                latitudes.add(cursor.getDouble(lat));
                longitudes.add(cursor.getDouble(lon));

                if(cursor.getInt(type) == Const.OrganizationState.FAVORITE) {
                    button_state.add(true);
                } else {
                    button_state.add(false);
                }

            } while(cursor.moveToNext());
        }

        distance = new ArrayList<>();
        for (int i = 0; i < latitudes.size(); i++) {
            double dist = round((((Math.acos(
                    Math.sin(latitude * Math.PI / 180) * Math.sin(latitudes.get(i) * Math.PI / 180) +
                    Math.cos(latitude * Math.PI / 180) * Math.cos(latitudes.get(i) * Math.PI / 180) *
                    Math.cos(Math.abs((longitude - longitudes.get(i))) * Math.PI / 180))) * 180 / Math.PI) * 60 * 1.1515 * 1.609344), 2);

            distance.add(dist);
        }

        for(int i = 0; i < distance.size(); i++) {
            for(int j = 0; j < distance.size(); j++) {
                if(distance.get(i) < distance.get(j)) {
                    Collections.swap(distance, i, j);
                    Collections.swap(latitudes, i, j);
                    Collections.swap(longitudes, i, j);
                    Collections.swap(button_state, i, j);
                    Collections.swap(organization, i, j);
                    Collections.swap(organization_id, i, j);
                    Collections.swap(addresses, i, j);
                }
            }
        }

        cursor.close();
        database.close();
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private void readDateBase(String name_find) {
        SQLiteDatabase database = DataBase.getDatabase();

        if (name_find.length() != 0) {
            name_find = "%" + name_find + "%";
        }

        Cursor cursor = database.query(DataBase.ORGANIZATIONS + " as " + DataBase.ORGANIZATIONS + " inner join " +
                        DataBase.ADDRESSES + " as " + DataBase.ADDRESSES + " on " +
                        DataBase.ORGANIZATIONS + "." + DataBase.ORGANIZATION_ID + " = " +
                        DataBase.ADDRESSES + "." + DataBase.ORGANIZATION_ID, // tables end

                new String[]{DataBase.ORGANIZATIONS + "." + DataBase.TITLE + " as " + DataBase.TITLE,
                        DataBase.ORGANIZATIONS + "." + DataBase.TITLE_LOWER + " as " + DataBase.TITLE_LOWER,
                        DataBase.ORGANIZATIONS + "." + DataBase.FAVORITE + " as " + DataBase.FAVORITE,
                        DataBase.ORGANIZATIONS + "." + DataBase.ORGANIZATION_ID + " as " + DataBase.ORGANIZATION_ID,
                        DataBase.ORGANIZATIONS + "." + DataBase.CATEGORY_ID + " as " + DataBase.CATEGORY_ID,
                        DataBase.ADDRESSES + "." + DataBase.FORMATTED_ADDRESS + " as " + DataBase.ADDRESS,
                        DataBase.ADDRESSES + "." + DataBase.LATITUDE + " as " + DataBase.LATITUDE,
                        DataBase.ADDRESSES + "." + DataBase.LONGITUDE + " as " + DataBase.LONGITUDE}, // columns end

                DataBase.ORGANIZATIONS + "." + DataBase.TITLE_LOWER + " like '" + name_find.toLowerCase() + "'",
                null, null, null, null);

        if(cursor.moveToFirst()) {
            int name = cursor.getColumnIndex(DataBase.TITLE);
            int type = cursor.getColumnIndex(DataBase.FAVORITE);
            int org_id = cursor.getColumnIndex(DataBase.ORGANIZATION_ID);
            int lat = cursor.getColumnIndex(DataBase.LATITUDE);
            int lon = cursor.getColumnIndex(DataBase.LONGITUDE);
            int addr = cursor.getColumnIndex(DataBase.ADDRESS);

            do {
                organization.add(cursor.getString(name));
                organization_id.add(cursor.getInt(org_id));
                addresses.add(cursor.getString(addr));
                latitudes.add(cursor.getDouble(lat));
                longitudes.add(cursor.getDouble(lon));

                if(cursor.getInt(type) == Const.OrganizationState.FAVORITE) {
                    button_state.add(true);
                } else {
                    button_state.add(false);
                }

            } while(cursor.moveToNext());
        }

        distance = new ArrayList<>();
        for (int i = 0; i < latitudes.size(); i++) {
            double dist = round((((Math.acos(
                    Math.sin(latitude * Math.PI / 180) * Math.sin(latitudes.get(i) * Math.PI / 180) +
                            Math.cos(latitude * Math.PI / 180) * Math.cos(latitudes.get(i) * Math.PI / 180) *
                                    Math.cos(Math.abs((longitude - longitudes.get(i))) * Math.PI / 180))) * 180 / Math.PI) * 60 * 1.1515 * 1.609344), 2);

            distance.add(dist);
        }

        for(int i = 0; i < distance.size(); i++) {
            for(int j = 0; j < distance.size(); j++) {
                if(distance.get(i) < distance.get(j)) {
                    Collections.swap(distance, i, j);
                    Collections.swap(latitudes, i, j);
                    Collections.swap(longitudes, i, j);
                    Collections.swap(button_state, i, j);
                    Collections.swap(organization, i, j);
                    Collections.swap(organization_id, i, j);
                    Collections.swap(addresses, i, j);
                }
            }
        }

        cursor.close();
        database.close();
    }

//    private class jsonParser extends AsyncTask<Void, Void, Bitmap> {
//
//        HttpURLConnection urlConnection = null;
//        BufferedReader reader = null;
//        String resultJson = "";
//
//        String find_name;
//
//        public jsonParser(String name) {
//            find_name = name;
//        }
//
//        @Override
//        protected Bitmap doInBackground(Void... params) {
//            String url_img = null;
//
//            try {
//                URL url = new URL("http://www.phonebook36.ru/api/?method=getBanner&query=" + URLEncoder.encode(find_name, "UTF-8"));
//
//                urlConnection = (HttpURLConnection) url.openConnection();
//                urlConnection.setRequestMethod("GET");
//                urlConnection.connect();
//
//                InputStream inputStream = urlConnection.getInputStream();
//                StringBuffer buffer = new StringBuffer();
//
//                reader = new BufferedReader(new InputStreamReader(inputStream));
//
//                String line;
//                while ((line = reader.readLine()) != null) {
//                    buffer.append(line);
//                }
//
//                resultJson = buffer.toString();
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//
//            Log.d("my", resultJson);
//
//            try {
//                JSONObject object = new JSONObject(resultJson);
//                url_img = object.getString("url");
//                banner_id = readId(url_img);
//                Log.d("my", url_img);
//
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//
//            Bitmap bmp = null;
//
//            try {
//                URL url = new URL(url_img);
//                bmp = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//
//            } catch (Exception e) {
//                Log.d("ImageManager", "Error: " + e.toString());
//            }
//
//            return bmp;
//        }
//
//        @Override
//        protected void onPostExecute(Bitmap bmp) {
//            super.onPostExecute(bmp);
//            logoImage.setImageBitmap(bmp);
//            if(bmp != null) {
//                logoImage.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.banner_height);
//                logoImage.requestLayout();
//            } else {
//                logoImage.getLayoutParams().height = 0;
//                logoImage.requestLayout();
//            }
//            logoImage.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
////                    BlurBehind.getInstance().execute(getActivity(), new OnBlurCompleteListener() {
////                        @Override
////                        public void onBlurComplete() {
////                            Intent intent = new Intent(getActivity(), organizationActivity.class);
////
////                            for(int i = 0; i < organization_id.size(); i++) {
////                                if(organization_id.get(i) == banner_id) {
////                                    intent.putExtra("subcategory_item_name", organization.get(i));
////                                }
////                            }
////
////                            intent.putExtra("id", banner_id);
////                            startActivity(intent);
////                        }
////                    });
//                }
//            });
//        }
//
//        private int readId(String name) {
//            SQLiteDatabase database = db.getReadableDatabase();
//            int id = 0;
//
//            Cursor cursor = database.query(DataBase.BANNER,
//                    new String[] {DataBase.BANNER_URL, DataBase.BANNER_ID},
//                    DataBase.BANNER_URL + " = " + "'" + name + "'", null, null, null, null);
//
//            if(cursor.moveToFirst()) {
//                int org_id = cursor.getColumnIndex(DataBase.BANNER_ID);
//                id = cursor.getInt(org_id);
//            }
//
//            cursor.close();
//            database.close();
//
//            return id;
//        }
//    }
//
//    public void updateBanner(String name) {
//        Log.d("subcategoriesPage", "updateBanner name=" + name);
//        logoImage.getLayoutParams().height = 0;
//        logoImage.requestLayout();
//        new jsonParser(name).execute();
//    }
//
//    public void readImageFromDataBase() {
//        SQLiteDatabase database = db.getReadableDatabase();
//        byte[] logo_img = null;
//
//        logoImage.getLayoutParams().height = 0;
//        logoImage.requestLayout();
//
//        for(int i = 0; i < organization_id.size(); i++) {
//            Cursor cursor = database.query(DataBase.BANNER,
//                    new String[] {DataBase.BANNER_ID, DataBase.BANNER_IMAGE},
//                    DataBase.BANNER_ID + " = " + Integer.toString(organization_id.get(i)), null, null, null, null);
//
//            if(cursor.moveToFirst()) {
//                int img = cursor.getColumnIndex(DataBase.BANNER_IMAGE);
//                int id = cursor.getColumnIndex(DataBase.BANNER_ID);
//
//                banner_id = cursor.getInt(id);
//                logo_img = cursor.getBlob(img);
//            }
//
//            if(logo_img != null) {
//                logoImage.setImageBitmap(BitmapFactory.decodeByteArray(logo_img, 0, logo_img.length));
//                logoImage.getLayoutParams().height = getResources().getDimensionPixelSize(R.dimen.banner_height);;
//                logoImage.requestLayout();
//
//                logoImage.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
////                        BlurBehind.getInstance().execute(getActivity(), new OnBlurCompleteListener() {
////                            @Override
////                            public void onBlurComplete() {
////                                Intent intent = new Intent(getActivity(), organizationActivity.class);
////
////                                for(int i = 0; i < organization_id.size(); i++) {
////                                    if(organization_id.get(i) == banner_id) {
////                                        intent.putExtra("subcategory_item_name", organization.get(i));
////                                    }
////                                }
////
////                                intent.putExtra("id", banner_id);
////                                startActivity(intent);
////                            }
////                        });
//                    }
//                });
//            }
//
//            cursor.close();
//        }
//
//        database.close();
//    }
}
