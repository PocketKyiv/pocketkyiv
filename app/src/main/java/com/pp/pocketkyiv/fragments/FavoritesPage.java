package com.pp.pocketkyiv.fragments;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.pp.pocketkyiv.App;
import com.pp.pocketkyiv.Const;
import com.pp.pocketkyiv.DataBase;
import com.pp.pocketkyiv.OrganizationActivity;
import com.pp.pocketkyiv.R;
import com.pp.pocketkyiv.adapters.ListViewFavoritesAdapter;
import com.pp.pocketkyiv.blur.BlurBehind;
import com.pp.pocketkyiv.blur.OnBlurCompleteListener;

import java.util.ArrayList;
import java.util.Collections;

public class FavoritesPage extends Fragment {

    private ListView listNames;
    private ArrayList<String> names;
    private ArrayList<Integer> item_id;
    private ArrayList<Integer> subcategory_id;
    private ArrayList<String> item_next_start;
    private ArrayList<Boolean> button_state;
    private ArrayList<String> addresses;
    private ArrayList<Double> distance;
    private ArrayList<Double> latitudes;
    private ArrayList<Double> longitudes;

    double latitude = 0;
    double longitude = 0;

    public FavoritesPage() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_favorites, container, false);

        checkLocation();

        names = new ArrayList<>();
        item_id = new ArrayList<>();
        button_state = new ArrayList<>();
        subcategory_id = new ArrayList<>();
        item_next_start = new ArrayList<>();
        distance = new ArrayList<>();
        latitudes = new ArrayList<>();
        longitudes = new ArrayList<>();
        addresses = new ArrayList<>();

        clearNextStart();
        readDataBase();

        for(int i = 0; i < names.size(); i++) {
            button_state.add(true);
        }

        final ListViewFavoritesAdapter adapter = new ListViewFavoritesAdapter(getActivity(), names,
                button_state, subcategory_id, distance, addresses);

        listNames = (ListView) rootView.findViewById(R.id.listView);
        listNames.setAdapter(adapter);

        listNames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                BlurBehind.getInstance().execute(getActivity(), new OnBlurCompleteListener() {
                    @Override
                    public void onBlurComplete() {
                        Intent intent = new Intent(getActivity(), OrganizationActivity.class);
                        intent.putExtra("subcategory_item_name", names.get(position));
                        intent.putExtra("id", item_id.get(position));
                        intent.putExtra("my_lat", latitude);
                        intent.putExtra("my_lon", longitude);
                        intent.putExtra("lat", latitudes.get(position));
                        intent.putExtra("lon", longitudes.get(position));

                        startActivity(intent);
                    }
                });
            }
        });

        return rootView;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if(requestCode == Const.PERMISSION_LOCATION) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkLocation();
            } else {
                getActivity().finish();
            }
        }
    }

    private void checkLocation() {
        LocationManager locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(App.getAppContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(App.getAppContext(),
                        Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            if(!App.getPermissionRequested()) {
                App.setPermissionRequested(true);
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                        Const.PERMISSION_LOCATION);
            }

        } else {

            locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    latitude = location.getLatitude();
                    longitude = location.getLongitude();
                }

                @Override
                public void onStatusChanged(String provider, int status, Bundle extras) {

                }

                @Override
                public void onProviderEnabled(String provider) {

                }

                @Override
                public void onProviderDisabled(String provider) {

                }
            });
        }
    }

    public void updatePage() {
        names.clear();
        item_id.clear();
        button_state.clear();
        subcategory_id.clear();
        item_next_start.clear();
        distance.clear();
        latitudes.clear();
        longitudes.clear();
        addresses.clear();
        readDataBaseFromUpdate();

        for(int i = 0; i < names.size(); i++) {
            if (names.get(i).equals(item_next_start.get(i))) {
                button_state.add(false);
            } else {
                button_state.add(true);
            }
        }

        final ListViewFavoritesAdapter adapter = new ListViewFavoritesAdapter(getActivity(),
                names, button_state, subcategory_id, distance, addresses);
        listNames.setAdapter(adapter);

        listNames.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
                BlurBehind.getInstance().execute(getActivity(), new OnBlurCompleteListener() {
                    @Override
                    public void onBlurComplete() {
                        Intent intent = new Intent(getActivity(), OrganizationActivity.class);
                        intent.putExtra("subcategory_item_name", names.get(position));
                        intent.putExtra("id", item_id.get(position));
                        intent.putExtra("my_lat", latitude);
                        intent.putExtra("my_lon", longitude);
                        intent.putExtra("lat", latitudes.get(position));
                        intent.putExtra("lon", longitudes.get(position));

                        startActivity(intent);
                    }
                });
            }
        });
    }

    private void readDataBase() {
        SQLiteDatabase database = DataBase.getDatabase();

        Cursor cursor = database.query(DataBase.ORGANIZATIONS + " as " + DataBase.ORGANIZATIONS + " inner join " +
                        DataBase.ADDRESSES + " as " + DataBase.ADDRESSES + " on " +
                        DataBase.ORGANIZATIONS + "." + DataBase.ORGANIZATION_ID + " = " +
                        DataBase.ADDRESSES + "." + DataBase.ORGANIZATION_ID, // tables end

                new String[]{DataBase.ORGANIZATIONS + "." + DataBase.TITLE + " as " + DataBase.TITLE,
                        DataBase.ORGANIZATIONS + "." + DataBase.FAVORITE + " as " + DataBase.FAVORITE,
                        DataBase.ORGANIZATIONS + "." + DataBase.ORGANIZATION_ID + " as " + DataBase.ORGANIZATION_ID,
                        DataBase.ORGANIZATIONS + "." + DataBase.CATEGORY_ID + " as " + DataBase.CATEGORY_ID,
                        DataBase.ADDRESSES + "." + DataBase.FORMATTED_ADDRESS + " as " + DataBase.ADDRESS,
                        DataBase.ADDRESSES + "." + DataBase.LATITUDE + " as " + DataBase.LATITUDE,
                        DataBase.ADDRESSES + "." + DataBase.LONGITUDE + " as " + DataBase.LONGITUDE}, // columns end

                DataBase.ORGANIZATIONS + "." + DataBase.FAVORITE + " = " +
                        Integer.toString(Const.OrganizationState.FAVORITE), null, null, null, null);

        if(cursor.moveToFirst()) {
            int name = cursor.getColumnIndex(DataBase.TITLE);
            int type = cursor.getColumnIndex(DataBase.FAVORITE);
            int org_id = cursor.getColumnIndex(DataBase.ORGANIZATION_ID);
            int lat = cursor.getColumnIndex(DataBase.LATITUDE);
            int lon = cursor.getColumnIndex(DataBase.LONGITUDE);
            int addr = cursor.getColumnIndex(DataBase.ADDRESS);
            int cat = cursor.getColumnIndex(DataBase.CATEGORY_ID);

            do {
                names.add(cursor.getString(name));
                item_id.add(cursor.getInt(org_id));
                subcategory_id.add(cursor.getInt(cat));
                addresses.add(cursor.getString(addr));
                latitudes.add(cursor.getDouble(lat));
                longitudes.add(cursor.getDouble(lon));

                if(cursor.getInt(type) == Const.OrganizationState.FAVORITE) {
                    button_state.add(true);
                } else {
                    button_state.add(false);
                }

            } while(cursor.moveToNext());
        }

        distance = new ArrayList<>();
        for (int i = 0; i < latitudes.size(); i++) {
            double dist = round((((Math.acos(
                    Math.sin(latitude * Math.PI / 180) * Math.sin(latitudes.get(i) * Math.PI / 180) +
                            Math.cos(latitude * Math.PI / 180) * Math.cos(latitudes.get(i) * Math.PI / 180) *
                                    Math.cos(Math.abs((longitude - longitudes.get(i))) * Math.PI / 180))) * 180 / Math.PI) * 60 * 1.1515 * 1.609344), 2);

            distance.add(dist);
        }

        for(int i = 0; i < distance.size(); i++) {
            for(int j = 0; j < distance.size(); j++) {
                if(distance.get(i) < distance.get(j)) {
                    Collections.swap(distance, i, j);
                    Collections.swap(latitudes, i, j);
                    Collections.swap(longitudes, i, j);
                    Collections.swap(button_state, i, j);
                    Collections.swap(names, i, j);
                    Collections.swap(item_id, i, j);
                    Collections.swap(subcategory_id, i, j);
                    Collections.swap(addresses, i, j);
                }
            }
        }

        cursor.close();
        database.close();
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private void readDataBaseFromUpdate() {
        SQLiteDatabase database = DataBase.getDatabase();

        Cursor cursor = database.query(DataBase.ORGANIZATIONS + " as " + DataBase.ORGANIZATIONS + " inner join " +
                        DataBase.ADDRESSES + " as " + DataBase.ADDRESSES + " on " +
                        DataBase.ORGANIZATIONS + "." + DataBase.ORGANIZATION_ID + " = " +
                        DataBase.ADDRESSES + "." + DataBase.ORGANIZATION_ID, // tables end

                new String[]{DataBase.ORGANIZATIONS + "." + DataBase.TITLE + " as " + DataBase.TITLE,
                        DataBase.ORGANIZATIONS + "." + DataBase.FAVORITE + " as " + DataBase.FAVORITE,
                        DataBase.ORGANIZATIONS + "." + DataBase.NEXT_START + " as " + DataBase.NEXT_START,
                        DataBase.ORGANIZATIONS + "." + DataBase.ORGANIZATION_ID + " as " + DataBase.ORGANIZATION_ID,
                        DataBase.ORGANIZATIONS + "." + DataBase.CATEGORY_ID + " as " + DataBase.CATEGORY_ID,
                        DataBase.ADDRESSES + "." + DataBase.FORMATTED_ADDRESS + " as " + DataBase.ADDRESS,
                        DataBase.ADDRESSES + "." + DataBase.LATITUDE + " as " + DataBase.LATITUDE,
                        DataBase.ADDRESSES + "." + DataBase.LONGITUDE + " as " + DataBase.LONGITUDE}, // columns end

                DataBase.ORGANIZATIONS + "." + DataBase.FAVORITE + " = " +
                        Integer.toString(Const.OrganizationState.FAVORITE) + " OR " + DataBase.ORGANIZATIONS + "." +
                        DataBase.NEXT_START + " = " + Integer.toString(Const.NextStart.DELETE_FROM_FAVORITE),
                null, null, null, null);

        if(cursor.moveToFirst()) {
            int name = cursor.getColumnIndex(DataBase.TITLE);
            int next = cursor.getColumnIndex(DataBase.NEXT_START);
            int org_id = cursor.getColumnIndex(DataBase.ORGANIZATION_ID);
            int lat = cursor.getColumnIndex(DataBase.LATITUDE);
            int lon = cursor.getColumnIndex(DataBase.LONGITUDE);
            int addr = cursor.getColumnIndex(DataBase.ADDRESS);
            int cat = cursor.getColumnIndex(DataBase.CATEGORY_ID);

            do {
                names.add(cursor.getString(name));
                item_id.add(cursor.getInt(org_id));
                subcategory_id.add(cursor.getInt(cat));
                addresses.add(cursor.getString(addr));
                latitudes.add(cursor.getDouble(lat));
                longitudes.add(cursor.getDouble(lon));

                if (cursor.getInt(next) == Const.NextStart.DELETE_FROM_FAVORITE) {
                    item_next_start.add(cursor.getString(name));
                } else {
                    item_next_start.add("");
                }

            } while(cursor.moveToNext());
        }

        distance = new ArrayList<>();
        for (int i = 0; i < latitudes.size(); i++) {
            double dist = round((((Math.acos(
                    Math.sin(latitude * Math.PI / 180) * Math.sin(latitudes.get(i) * Math.PI / 180) +
                            Math.cos(latitude * Math.PI / 180) * Math.cos(latitudes.get(i) * Math.PI / 180) *
                                    Math.cos(Math.abs((longitude - longitudes.get(i))) * Math.PI / 180))) * 180 / Math.PI) * 60 * 1.1515 * 1.609344), 2);

            distance.add(dist);
        }

        for(int i = 0; i < distance.size(); i++) {
            for(int j = 0; j < distance.size(); j++) {
                if(distance.get(i) < distance.get(j)) {
                    Collections.swap(distance, i, j);
                    Collections.swap(latitudes, i, j);
                    Collections.swap(longitudes, i, j);
                    Collections.swap(item_next_start, i, j);
                    Collections.swap(names, i, j);
                    Collections.swap(item_id, i, j);
                    Collections.swap(subcategory_id, i, j);
                    Collections.swap(addresses, i, j);
                }
            }
        }

        cursor.close();
        database.close();
    }

    private void clearNextStart() {
        SQLiteDatabase database = DataBase.getDatabase();
        Cursor cursor = database.query(DataBase.ORGANIZATIONS,
                new String[] {DataBase.NEXT_START},
                DataBase.NEXT_START + " = " + Integer.toString(Const.NextStart.DELETE_FROM_FAVORITE), null, null, null, null);

        ContentValues contentValues = new ContentValues();
        contentValues.put(DataBase.NEXT_START, Const.NextStart.NORMAL);

        if(cursor.moveToFirst()) {

            do {
                database.update(DataBase.ORGANIZATIONS, contentValues, null, null);

            } while(cursor.moveToNext());
        }

        cursor.close();
        database.close();
    }
}
