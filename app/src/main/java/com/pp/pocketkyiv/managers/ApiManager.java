package com.pp.pocketkyiv.managers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.pp.pocketkyiv.DataBase;
import com.pp.pocketkyiv.interfaces.IOnLastUpdateChange;
import com.pp.pocketkyiv.models.AddressModel;
import com.pp.pocketkyiv.models.CategoryModel;
import com.pp.pocketkyiv.models.OrganizationModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class ApiManager {

    private static final String API_KEY = "b18107f20d2cfe88c90449b4ed5187c4";
    private static final String GET_CATEGORIES = "http://pocketkyiv.pp.ua/api/get_categories";
    private static final String GET_ORGANIZATION = "http://pocketkyiv.pp.ua/api/get_organizations";
    private static final String GET_ADDRESSES = "http://pocketkyiv.pp.ua/api/get_addresses";
    private static final String LAST_UPDATE = "http://pocketkyiv.pp.ua/api/get_last_update";

    private static ApiManager instance;
    private AsyncHttpClient client;

    private ApiManager() {
        client = new AsyncHttpClient();
    }

    public synchronized static ApiManager getInstance() {
        if(instance == null)
            instance = new ApiManager();

        return instance;
    }

    public void getCategories(final Context context, final Runnable result) {
        RequestParams params = new RequestParams();
        params.put("api_key", API_KEY);

        client.post(context, GET_CATEGORIES, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(statusCode == 200) {
                    if(responseBody != null) {
                        final String response = new String(responseBody);
                        Log.d("onSuccess", "response = " + response);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<CategoryModel> models = new ArrayList<>();

                                try {
                                    JSONArray categories = new JSONArray(response);

                                    for(int i = 0; i < categories.length(); i++) {
                                        JSONObject category = categories.getJSONObject(i);
                                        models.add(new CategoryModel(category));
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                DataBase.writeCategories(models);
                                result.run();
                            }

                        }).start();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody != null) {
                    final String response = new String(responseBody);
                    Log.d("onFailure", "response = " + response);
                }

                failedUpdateDatabase(context);
            }
        });
    }

    public void getOrganizations(final Context context, final Runnable result) {
        RequestParams params = new RequestParams();
        params.put("api_key", API_KEY);

        client.post(context, GET_ORGANIZATION, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(statusCode == 200) {
                    if(responseBody != null) {
                        final String response = new String(responseBody);
                        Log.d("onSuccess", "response = " + response);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<OrganizationModel> models = new ArrayList<>();

                                try {
                                    JSONArray organizations = new JSONArray(response);

                                    for(int i = 0; i < organizations.length(); i++) {
                                        JSONObject organization = organizations.getJSONObject(i);
                                        models.add(new OrganizationModel(organization));
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                DataBase.writeOrganizations(models);
                                result.run();
                            }

                        }).start();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody != null) {
                    String response = new String(responseBody);
                    Log.d("onFailure", "response = " + response);
                }

                failedUpdateDatabase(context);
            }
        });
    }

    public void getAddresses(final Context context, final Runnable result) {
        RequestParams params = new RequestParams();
        params.put("api_key", API_KEY);

        client.post(context, GET_ADDRESSES, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(statusCode == 200) {
                    if(responseBody != null) {
                        final String response = new String(responseBody);
                        Log.d("onSuccess", "response = " + response);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<AddressModel> models = new ArrayList<>();

                                try {
                                    JSONArray addresses = new JSONArray(response);

                                    for(int i = 0; i < addresses.length(); i++) {
                                        JSONObject address = addresses.getJSONObject(i);
                                        models.add(new AddressModel(address));
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                DataBase.writeAddresses(models);
                                result.run();
                            }

                        }).start();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody != null) {
                    String response = new String(responseBody);
                    Log.d("onFailure", "response = " + response);
                }

                failedUpdateDatabase(context);
            }
        });
    }

    public void getLastUpdate(final Context context, final IOnLastUpdateChange result) {
        RequestParams params = new RequestParams();
        params.put("api_key", API_KEY);

        client.post(context, LAST_UPDATE, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(statusCode == 200) {
                    if(responseBody != null) {
                        final String response = new String(responseBody);
                        Log.d("onSuccess", "response = " + response);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                long organizations = 0;
                                long addresses = 0;

                                try {
                                    JSONObject dates = new JSONObject(response);
                                    organizations = dates.getLong(DataBase.ORGANIZATIONS);
                                    addresses = dates.getLong(DataBase.ADDRESSES);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                result.lastUpdateDate(organizations, addresses);
                            }

                        }).start();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody != null) {
                    String response = new String(responseBody);
                    Log.d("onFailure", "response = " + response);
                }

                failedUpdateDatabase(context);
            }
        });
    }

    public void updateOrganizations(final Context context, long organizations, final Runnable result) {
        RequestParams params = new RequestParams();
        params.put("api_key", API_KEY);
        params.put("last_update", organizations);

        client.post(context, GET_ORGANIZATION, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(statusCode == 200) {
                    if(responseBody != null) {
                        final String response = new String(responseBody);
                        Log.d("onSuccess", "response = " + response);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<OrganizationModel> models = new ArrayList<>();

                                try {
                                    JSONArray organizations = new JSONArray(response);

                                    for(int i = 0; i < organizations.length(); i++) {
                                        JSONObject organization = organizations.getJSONObject(i);
                                        models.add(new OrganizationModel(organization));
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                DataBase.updateOrganizations(models);
                                result.run();
                            }

                        }).start();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody != null) {
                    String response = new String(responseBody);
                    Log.d("onFailure", "response = " + response);
                }

                failedUpdateDatabase(context);
            }
        });
    }

    public void updateAddresses(final Context context, long address, final Runnable result) {
        RequestParams params = new RequestParams();
        params.put("api_key", API_KEY);
        params.put("last_update", address);

        client.post(context, GET_ADDRESSES, params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if(statusCode == 200) {
                    if(responseBody != null) {
                        final String response = new String(responseBody);
                        Log.d("onSuccess", "response = " + response);

                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                ArrayList<AddressModel> models = new ArrayList<>();

                                try {
                                    JSONArray addresses = new JSONArray(response);

                                    for(int i = 0; i < addresses.length(); i++) {
                                        JSONObject address = addresses.getJSONObject(i);
                                        models.add(new AddressModel(address));
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                DataBase.updateAddresses(models);
                                result.run();
                            }

                        }).start();
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                if(responseBody != null) {
                    String response = new String(responseBody);
                    Log.d("onFailure", "response = " + response);
                }

                failedUpdateDatabase(context);
            }
        });
    }

    private void failedUpdateDatabase(final Context context) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, "Не могу обновить базу даних!", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
