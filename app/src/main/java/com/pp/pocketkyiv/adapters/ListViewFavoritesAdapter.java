package com.pp.pocketkyiv.adapters;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.pp.pocketkyiv.Const;
import com.pp.pocketkyiv.DataBase;
import com.pp.pocketkyiv.MainActivity;
import com.pp.pocketkyiv.R;

import java.util.ArrayList;

public class ListViewFavoritesAdapter extends BaseAdapter {

    private ArrayList<String> items;
    private ArrayList<Boolean> states;
    private ArrayList<Integer> item_id;
    private ArrayList<Double> distance;
    private ArrayList<String> addresses;
    private Context applicationContext;

    public ListViewFavoritesAdapter(Context context, ArrayList<String> name, ArrayList<Boolean> state,
                                    ArrayList<Integer> id, ArrayList<Double> distance, ArrayList<String> addresses) {
        items = name;
        states = state;
        item_id = id;
        this.distance = distance;
        this.addresses = addresses;
        applicationContext = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        TextView name;
        final ToggleButton button;

        LayoutInflater inflater = (LayoutInflater) applicationContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.list_view_item, null);

        name = (TextView) view.findViewById(R.id.organization_name);
        button = (ToggleButton) view.findViewById(R.id.favoritesButton);
        ((TextView) view.findViewById(R.id.distance)).setText(Double.toString(distance.get(position)) + " км");
        ((TextView) view.findViewById(R.id.address)).setText(addresses.get(position));

        name.setText(items.get(position));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(states.get(position)) {

                    button.setChecked(false);
                    states.set(position, false);
                    deleteFromDataBaseType(position);

                } else {

                    button.setChecked(true);
                    states.set(position, true);
                    writeToDataBaseType(position);

                }

                MainActivity main = (MainActivity) applicationContext;
                main.updateSubcategories(item_id.get(position));
            }
        });

        button.setChecked(states.get(position));

        return view;
    }

    private void writeToDataBaseType(int position) {
        SQLiteDatabase database = DataBase.getDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(DataBase.FAVORITE, Const.OrganizationState.FAVORITE);
        contentValues.put(DataBase.NEXT_START, Const.NextStart.NORMAL);
        database.update(DataBase.ORGANIZATIONS, contentValues,
                DataBase.TITLE + " = " + "\'" + items.get(position) + "\'", null);

        database.close();
    }

    private void deleteFromDataBaseType(int position) {
        SQLiteDatabase database = DataBase.getDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(DataBase.FAVORITE, Const.OrganizationState.NORMAL);
        contentValues.put(DataBase.NEXT_START, Const.NextStart.DELETE_FROM_FAVORITE);
        database.update(DataBase.ORGANIZATIONS, contentValues,
                DataBase.TITLE + " = " + "\'" + items.get(position) + "\'", null);

        database.close();
    }
}