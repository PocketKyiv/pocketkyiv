package com.pp.pocketkyiv.adapters;

import android.content.Context;
import android.content.res.Resources;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.pp.pocketkyiv.R;
import com.pp.pocketkyiv.fragments.ContentPage;
import com.pp.pocketkyiv.fragments.FavoritesPage;
import com.pp.pocketkyiv.fragments.SubcategoriesPage;

public class PagerAdapter extends FragmentPagerAdapter {

    private String[] names;

    private ContentPage content;
    private FavoritesPage favorites;
    private SubcategoriesPage subcategories;

    public PagerAdapter(FragmentManager fm, Context context) {
        super(fm);

        Resources resources = context.getResources();
        names = resources.getStringArray(R.array.names);

        content = new ContentPage();
        favorites = new FavoritesPage();
        subcategories = new SubcategoriesPage();
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return favorites;
            case 1:
                return content;
            case 2:
                return subcategories;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return names[position];
    }

    public void setNames(String name) {
        names[2] = name;
    }
}
