package com.pp.pocketkyiv;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.Gravity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pp.pocketkyiv.models.OrganizationModel;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class InformationActivity extends Activity {

    private String name = null;
    private String address = null;
    private String work = null;
    private String contact = null;
    private String email = null;
    private String site = null;

    private ArrayList<String> phones;
    private ArrayList<String> phones_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information);

        phones = new ArrayList<>();
        phones_name = new ArrayList<>();

        Intent intent = getIntent();
        readDataBase(intent.getIntExtra("id", 0));

        findViewById(R.id.inf_back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        TextView information_name = (TextView) findViewById(R.id.information_name);
        information_name.setText(name);

        FrameLayout information_address = (FrameLayout) findViewById(R.id.information_address);
        if (address != null && !address.equals("")) {
            TextView view = new TextView(this);
            view.setTextSize(16);
            view.setTextColor(getResources().getColor(R.color.pageTitleColor));
            view.setText(address);
            view.setGravity(Gravity.CENTER);
            view.setTypeface(null, Typeface.BOLD);
            information_address.addView(view);
        }

        LinearLayout information_work = (LinearLayout) findViewById(R.id.information_work);
        if (work != null && !work.equals("")) {
            TextView view = new TextView(this);
            view.setText(work);
            view.setTextSize(16);
            view.setTypeface(null, Typeface.BOLD);
            view.setTextColor(getResources().getColor(R.color.pageTitleColor));
            information_work.addView(view);
        }

        LinearLayout information_contact = (LinearLayout) findViewById(R.id.information_contact);
        if (contact != null && !contact.equals("")) {
            TextView view = new TextView(this);
            view.setText(contact);
            view.setTextSize(16);
            view.setTypeface(null, Typeface.BOLD);
            view.setTextColor(getResources().getColor(R.color.pageTitleColor));
            information_contact.addView(view);
        }

        FrameLayout information_email = (FrameLayout) findViewById(R.id.information_email);
        if (email != null && !email.equals("")) {
            TextView view = new TextView(this);
            view.setText(email);
            view.setTextSize(16);
            view.setTypeface(null, Typeface.BOLD);
            view.setTextColor(getResources().getColor(R.color.pageTitleColor));
            information_email.addView(view);
        }

        FrameLayout information_site = (FrameLayout) findViewById(R.id.information_site);
        if(site != null && !site.equals("")) {
            TextView view = new TextView(this);
            view.setText(site);
            view.setTextSize(16);
            view.setTypeface(null, Typeface.BOLD);
            view.setTextColor(getResources().getColor(R.color.pageTitleColor));
            information_site.addView(view);
        }

        LinearLayout information_phones = (LinearLayout) findViewById(R.id.information_phones);
        if(!phones.isEmpty()) {
            for (int i = 0; i < phones.size(); i++) {
                TextView view = new TextView(this);
                view.setTextSize(16);
                view.setTypeface(null, Typeface.BOLD);
                view.setTextColor(getResources().getColor(R.color.pageTitleColor));

                if(phones_name.get(i) == null) {
                    view.setText(phones.get(i));
                } else {
                    view.setText(phones.get(i) + " \\ " + phones_name.get(i));
                }

                information_phones.addView(view);
            }
        }

        findViewById(R.id.phone).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(InformationActivity.this);
                ArrayList<String> data = new ArrayList<>();

                for(int i = 0; i < phones.size(); i++) {
                    if(phones_name.get(i) == null) {
                        data.add(phones.get(i));
                    } else {
                        data.add(phones.get(i) + " \\ " + phones_name.get(i));
                    }
                }

                ArrayAdapter<String> adapter = new ArrayAdapter<>(InformationActivity.this,
                        android.R.layout.select_dialog_item, data);

                builder.setTitle("Выбор телефона");
                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        String telefon = "tel:" + phones.get(which);
                        callIntent.setData(Uri.parse(telefon));

                        if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                                Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                            return;
                        }

                        startActivity(callIntent);
                    }
                });

                builder.create().show();
            }
        });
    }

    private void readDataBase(int id) {
        SQLiteDatabase database = DataBase.getDatabase();

        Cursor cursor = database.query(DataBase.ORGANIZATIONS, new String[] {DataBase.PHONE_NUMBERS},
                DataBase.ORGANIZATION_ID + " = " + Integer.toString(id), null, null, null, null);

        if(cursor.moveToFirst()) {
            int phone = cursor.getColumnIndex(DataBase.PHONE_NUMBERS);
            try {
                ArrayList<OrganizationModel.Phone> phonesModel  = OrganizationModel
                        .parsePhones(new JSONArray(cursor.getString(phone)));
                for (OrganizationModel.Phone ph : phonesModel) {
                    phones.add(ph.number);
                    phones_name.add(ph.title);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        Cursor cursor_item = database.query(DataBase.ORGANIZATIONS, null,
                DataBase.ORGANIZATION_ID + " = " + Integer.toString(id), null, null, null, null);
        Cursor cursor_address = database.query(DataBase.ADDRESSES, null,
                DataBase.ORGANIZATION_ID + " = " + Integer.toString(id), null, null, null, null);

        if(cursor_item.moveToFirst() && cursor_address.moveToFirst()) {
            int nam = cursor_item.getColumnIndex(DataBase.TITLE);
            int addr = cursor_address.getColumnIndex(DataBase.FORMATTED_ADDRESS);
            int wor = cursor_item.getColumnIndex(DataBase.WORK_TIME);
            int pau = cursor_item.getColumnIndex(DataBase.CONTACT);
            int em = cursor_item.getColumnIndex(DataBase.EMAIL);
            int abou = cursor_item.getColumnIndex(DataBase.SITE);

            address = cursor_address.getString(addr);
            name = cursor_item.getString(nam);
            work = cursor_item.getString(wor);
            contact = cursor_item.getString(pau);
            email = cursor_item.getString(em);
            site = cursor_item.getString(abou);
        }

        cursor.close();
        cursor_address.close();
        cursor_item.close();
        database.close();
    }
}
