package com.pp.pocketkyiv.blur;

public interface OnBlurCompleteListener {
    void onBlurComplete();
}
