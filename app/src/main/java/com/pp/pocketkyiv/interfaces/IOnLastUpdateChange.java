package com.pp.pocketkyiv.interfaces;

public interface IOnLastUpdateChange {
    void lastUpdateDate(long organization, long address);
}
